# GitLab auto-deactivate inactive users

Provides a script to deactivate all inactive users using the GitLab API. Can be run as scheduled pipeline to continuously deactivate inactive users.

## Configuration

- Export / fork repository.
- Add a GitLab **admin** API token to CI/CD variables named `GITLAB_TOKEN`. Make sure it is "masked".
This token will be used to query the API for group and project members.
The token **must** be an admin token to be able to list all users and deactivate them.
- Add your GitLab instance URL to CI/CD variables named `GITLAB_URL`.
- notice the Job is tagged "local" for testing purposes. Make sure your runners pick it up.
- Schedule the pipeline to run it weekly or your desired interval, using Pipelines -> Schedules
- Upvote this issue: https://gitlab.com/gitlab-org/gitlab/-/issues/211754

## Usage

`python3 auto-deactivate-inactive-users.py $GITLAB_URL $GITLAB_TOKEN`

## Parameters

- `--dryrun`, `-d`: Dryrun mode. Only compile the CSV report of users to deactivate, don't actually deactivate anyone.

## What does it do?

* Request all users using https://docs.gitlab.com/ee/api/users.html#for-admins
* For all active users:
  * if user did not log in for 180 days or did never login: deactivate user
* If users were deactivated:
  * Write a CSV report of all deactivated users
